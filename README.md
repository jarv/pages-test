View site: 
* https://jarv.staging.gitlab.io/pages-test/
* http://jarv.staging.gitlab.io/pages-test/

----

# GitLab Pages - Plain HTML Example

## Template: [ArtCore](http://www.templatemo.com/tm-423-artcore)

## Distributor: [Templatemo](http://www.templatemo.com/)

## Configuration

- Default
- GitLab CI: [`.gitlab-ci.yml`](https://gitlab.com/html-themes/artcore/blob/master/.gitlab-ci.yml)

## More Info

- View [Wiki](https://gitlab.com/html-themes/artcore/wikis/home)